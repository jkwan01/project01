/**
 * JUnit tests for the Normalizer class
 * @author Jackson Kwan
 */
package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import movies.importer.*;

import java.util.*;

class NormalizerTests {

	/**
	 * test for the process method
	 */
	@Test
	public void processTest() {
		Normalizer tester = new Normalizer("D:\\school\\java310\\Project_1_MovieImporter\\project1testersrc", "D:\\school\\java310\\Project_1_MovieImporter\\project1testerdest");
		ArrayList<String> testList = new ArrayList<String>();
		String header = "Release Year\tTitle\tRuntime\tSource";
		String line1 = "2008\tThe Mummy: Tomb of the Dragon Emperor\t112 minutes\tkaggle";
		testList.add(header);
		testList.add(line1);
		assertEquals("2008\tthe mummy: tomb of the dragon emperor\t112\tkaggle", tester.process(testList).get(1));
	}

}
