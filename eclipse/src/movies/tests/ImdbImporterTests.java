/**
 * Test Units for the ImdbImporter Class
 * @author David Tran
 */

package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.*;

class ImdbImporterTests {

	/**
	 * Unit Tests for the hardcoded variables
	 */
	@Test
	public void variablesTests() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		ImdbImporter newImdbImporter = new ImdbImporter(source, destination);
		
		assertEquals(1, newImdbImporter.movieTitleIndex);
		assertEquals(3, newImdbImporter.releaseYearIndex);
		assertEquals(6, newImdbImporter.runTimeIndex);
		assertEquals("Imdb", newImdbImporter.source);
	}

	/**
	 * Unit Tests for the expected output format of the ImdbImporter process 
	 * (The String added to the ArrayList for the tests are from the files since the index of the movie title, release year and the runtime are hardcoded)
	 */
	@Test
	public void processTests() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		ImdbImporter newImdbImporter = new ImdbImporter(source, destination);
		ArrayList<String> input = new ArrayList<String>();
		String header = "imdb_title_id	title	original_title	year	date_published	genre	duration	country	language	director	writer	production_company	actors	description	avg_vote	votes	budget	usa_gross_income	worlwide_gross_income	metascore	reviews_from_users	reviews_from_critics";
		String lineOne = "tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2";
		
		input.add(header);
		input.add(lineOne);
				
		assertEquals("year\ttitle\tduration\tImdb", newImdbImporter.process(input).get(0));
		assertEquals("1894\tMiss Jerry\t45\tImdb", newImdbImporter.process(input).get(1));
		
	}
	
}