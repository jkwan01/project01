/**
 * Main Application of the movie import pipeline
 * @author Jackson Kwan and David Tran
 */

package movies.importer;

import java.io.IOException;

public class ImportPipeline {
	public static void main(String[]args) throws IOException {
		Processor[] processor = new Processor[5];
		String kaggleImporterSource = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\KaggleSource";
		String imdbImporterSource = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\ImdbSource";
		String importerDestination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Norm";
		String normalizerSource = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Norm";
		String normalizerDestination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Vald";
		String validatorSource = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Vald";
		String validatorDestination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\ded";
		String deduperSource = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\ded";
		String deduperDestination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		
		ImdbImporter newImdbImporter = new ImdbImporter(imdbImporterSource,importerDestination);
		KaggleImporter newKaggleImporter = new KaggleImporter(kaggleImporterSource,importerDestination);
		Normalizer newNormalizer = new Normalizer(normalizerSource, normalizerDestination);
		Validator newValidator = new Validator(validatorSource, validatorDestination);
		Deduper newDeduper = new Deduper(deduperSource, deduperDestination);
		
		processor[0] = newImdbImporter;
		processor[1] = newKaggleImporter;
		processor[2] = newNormalizer;
		processor[3] = newValidator;
		processor[4] = newDeduper;
		
		processAll(processor);
	}
	
	public static void processAll(Processor[] processor) throws IOException {
		
		for (int i = 0; i < processor.length; i++) {
			processor[i].execute();
		}
		
	}
}
