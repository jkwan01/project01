/**
 * Normalizes the inputted file so that every file has the same format
 * @author Jackson Kwan
 */
package movies.importer;
import java.util.*;

public class Normalizer extends Processor{
	
	/**
	 * Normalizer constructor
	 * @param src source of file
	 * @param dest destination of normalized file
	 */
	public Normalizer(String src, String dest) {
		super(src, dest, false);
	}
	
	/**
	 * Implementation of inherited method process from Processor
	 * @param input arraylist of data to be normalized
	 * @return normalized arraylist containing the input data
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> returnList = new ArrayList<String>();
		for (int i=0; i<input.size();i++) {
			String[] tempStrArr = input.get(i).split("\\t");
			final int releaseYearIndex = 0;
			final int runtimeIndex = 2;
			final int titleIndex = 1;
			final int sourceIndex = 3;
			Movie tempMovie = new Movie(tempStrArr[releaseYearIndex], tempStrArr[titleIndex], tempStrArr[runtimeIndex], tempStrArr[sourceIndex]);
			tempMovie.firstWord();
			tempMovie.lowered();
			returnList.add(tempMovie.toString());
			}
		return returnList;
		}
	}

