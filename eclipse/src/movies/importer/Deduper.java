/**
 * Deduper class that merges the same movies with less than 5 minutes runtime difference
 * @author Jackson Kwan and David Tran
 */

package movies.importer;
import java.util.*;



public class Deduper extends Processor{
	
	/**
	 * Deduper constructor
	 * @param src file source
	 * @param dest file destination
	 */
	public Deduper(String src, String dest) {
		super(src, dest, false);
	}
	
	/**
	 * process inheritance method implementation to dedupe
	 * @param input normalized and validated file to be deduped
	 * @return newMerge deduped arraylist of movies
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> newMerge = new ArrayList<String>();
		ArrayList<Movie> movieList = new ArrayList<Movie>();
		final int releaseDateIndex = 0;
		final int runtimeIndex = 2;
		final int titleIndex = 1;
		final int sourceIndex = 3;
		int similarIndex = 0;
		
		for(int i = 0; i < input.size(); i++) {
			String[] converted = input.get(i).split("\\t");

			String movieSource = converted[sourceIndex];
			Movie tempMovie = new Movie(converted[releaseDateIndex], converted[titleIndex], converted[runtimeIndex], converted[sourceIndex]);
			if(movieList.contains(tempMovie)) {

				for (int j=0; j<movieList.size(); j++) {
					if (movieList.get(j).getMovieName().equals(tempMovie.getMovieName())) {
						similarIndex = j;
					}
				}
				
				if (movieList.get(similarIndex).getSource() != tempMovie.getSource()) {
					movieSource = "imdb;kaggle";
				}
				Movie merged = new Movie(tempMovie.getReleaseYear(), tempMovie.getMovieName(), tempMovie.getRuntime(), movieSource);
				movieList.set(similarIndex, merged);
			}
			else {
				movieList.add(tempMovie);
			}
		}
		
		
		
		for (int i = 0; i < movieList.size(); i++) {
			newMerge.add(movieList.get(i).toString());
		}
		
		return newMerge;
	}
}
