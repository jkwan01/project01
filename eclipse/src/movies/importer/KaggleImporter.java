/**
 * Importer for the kaggle file
 * @author Jackson Kwan
 */
package movies.importer;
import java.util.*;

public class KaggleImporter extends Processor{
	
	/**
	 * Constructor for the KaggleImporter class
	 * @param src file source
	 * @param dest destination of import
	 */
	public KaggleImporter(String src, String dest) {
		super(src, dest, true);
	}
	/**
	 * Implementation of the inherited process method
	 * @param input arraylist to be imported to proper style
	 * @return returnList arraylist of imported toStrings from input
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> returnList = new ArrayList<String>();
		for (int i=0; i<input.size();i++) {
			String[] tempStrArr = input.get(i).split("\\t");
			final int releaseYearIndex = 20;
			final int runtimeIndex = 13;
			final int titleIndex = 15;
			Movie tempMovie = new Movie(tempStrArr[releaseYearIndex], tempStrArr[titleIndex], tempStrArr[runtimeIndex], "kaggle");
			returnList.add(tempMovie.toString());
		}
		return returnList;
	}
}
